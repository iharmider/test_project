<?php

/**
 * Root path to site directory
 */
define('FS_ROOT', dirname(__FILE__)."/");

define("FS_LIBS", FS_ROOT."libs/");

define('AUTH_DATA', 'dbadmin');
define('AUTH_TOKEN', 'zero');

/**
 * This is key in get params for defined redirect with restore data from session
 */
define('REDIRECT_POINT_KEY_IN_REQUEST', 'sh');

if (empty($GLOBALS['config']['storage_path'])) {
    $GLOBALS['config']['storage_path']  = FS_ROOT.'static/storage';
}

$GLOBALS['config']['storage_url']   = '/static/storage';

$GLOBALS['config']['paths'] = array(
    'plugins'      => FS_ROOT.'plugins/',
    'objects'      => FS_ROOT.'objects/',
    'logs'         => FS_ROOT.'logs/',
);

$GLOBALS['config']['http_paths'] = array(
);

$GLOBALS['config']['db']['dsn']  = 'mysql:dbname=devs_iharmider_test_project;host=localhost';
$GLOBALS['config']['db']['user'] = 'test_project';
$GLOBALS['config']['db']['pass'] = '6D0r6P0o';

$GLOBALS['pluginRules'] = array();

// Hooks
$GLOBALS['config']['hooks'] = array(
);

date_default_timezone_set('Europe/Berlin');

error_reporting(E_ALL);
ini_set('display_errors', 1);

$localConfigPath = FS_ROOT.'local.php';
if (file_exists($localConfigPath)) {
    require $localConfigPath;
}

ini_set('error_log', $GLOBALS['config']['paths']['logs'].'php.log');

include_once FS_ROOT."common.php";
