<?php
try {
    require_once __DIR__.'/config.php';

    $options = array(
       'convert_path'  => '/usr/bin/convert',
       'theme_name'    => 'default',
       'engine_folder' => 'core'
    );
    
    /**
     * @global Controller $GLOBALS['controller']
     * @name $controller
     */
    $controller = Controller::getInstance($options);
    
    $controller->config = $GLOBALS['config'];

    $controller->db = Object::factory($db);

    $controller->user = new DefaultUser($GLOBALS['_sessionData']);


    $systemPlugin = $controller->getPluginInstance('Jimbo');
    
    $controller->setSystemPlugin($systemPlugin->getInstance());
     
    
    $options = array(
        'onBind' => array($systemPlugin, 'onBind'),
        'area' => 'backend'
    );
    
    $systemPlugin->bindRequest($options);

} catch (Exception $exp) {
    echo $exp->getMessage();
}